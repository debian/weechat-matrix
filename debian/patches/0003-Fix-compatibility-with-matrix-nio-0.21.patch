From: Simon Chopin <simon.chopin@canonical.com>
Date: Tue, 18 Jul 2023 14:04:26 +0200
Subject: Fix compatibility with matrix-nio 0.21

The 0.21.0 made a breaking change in how they handle logging, moving off
logbook to the standard logging module, breaking weechat-matrix's config
module.

This patch adresses the API change (without migrating ourselves to
logboox), and bumps the matrix-nio requirements to reflect the
dependency on the new API.

Signed-off-by: Simon Chopin <simon.chopin@canonical.com>
---
 main.py           | 26 ++++++--------------------
 matrix/config.py  | 45 +++++++++++++++++++--------------------------
 matrix/globals.py |  4 ++--
 pyproject.toml    |  3 +--
 requirements.txt  |  3 +--
 5 files changed, 29 insertions(+), 52 deletions(-)

diff --git a/main.py b/main.py
index 765043a..91838b6 100644
--- a/main.py
+++ b/main.py
@@ -29,6 +29,7 @@ activate_this = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'venv'
 if os.path.exists(activate_this):
     exec(open(activate_this).read(), {'__file__': activate_this})
 
+import logging
 import socket
 import ssl
 import textwrap
@@ -38,11 +39,9 @@ from itertools import chain
 # pylint: disable=unused-import
 from typing import Any, AnyStr, Deque, Dict, List, Optional, Set, Text, Tuple
 
-import logbook
 import json
 import OpenSSL.crypto as crypto
 from future.utils import bytes_to_native_str as n
-from logbook import Logger, StreamHandler
 
 try:
     from json.decoder import JSONDecodeError
@@ -114,7 +113,7 @@ WEECHAT_SCRIPT_LICENSE = "ISC"                                 # type: str
 # yapf: enable
 
 
-logger = Logger("matrix-cli")
+logger = logging.getLogger(__name__)
 
 
 def print_certificate_info(buff, sock, cert):
@@ -532,20 +531,8 @@ def server_buffer_cb(server_name, buffer, input_data):
     return W.WEECHAT_RC_OK
 
 
-class WeechatHandler(StreamHandler):
-    def __init__(self, level=logbook.NOTSET, format_string=None, filter=None,
-                 bubble=False):
-        StreamHandler.__init__(
-            self,
-            object(),
-            level,
-            format_string,
-            None,
-            filter,
-            bubble
-        )
-
-    def write(self, item):
+class WeechatHandler(logging.StreamHandler):
+    def emit(self, record):
         buf = ""
 
         if G.CONFIG.network.debug_buffer:
@@ -555,7 +542,7 @@ class WeechatHandler(StreamHandler):
 
             buf = G.CONFIG.debug_buffer
 
-        W.prnt(buf, item)
+        W.prnt(buf, record)
 
 
 def buffer_switch_cb(_, _signal, buffer_ptr):
@@ -688,8 +675,7 @@ if __name__ == "__main__":
             W.prnt("", message)
 
         handler = WeechatHandler()
-        handler.format_string = "{record.channel}: {record.message}"
-        handler.push_application()
+        logger.addHandler(handler)
 
         # TODO if this fails we should abort and unload the script.
         G.CONFIG = MatrixConfig()
diff --git a/matrix/config.py b/matrix/config.py
index f4ff40a..33063e9 100644
--- a/matrix/config.py
+++ b/matrix/config.py
@@ -24,12 +24,11 @@ To add configuration options refer to MatrixConfig.
 Server specific configuration options are handled in server.py
 """
 
+import logging
 from builtins import super
 from collections import namedtuple
 from enum import IntEnum, Enum, unique
 
-import logbook
-
 import nio
 from matrix.globals import SCRIPT_NAME, SERVERS, W
 from matrix.utf import utf8_decode
@@ -57,9 +56,8 @@ class NewChannelPosition(IntEnum):
     NEXT = 1
     NEAR_SERVER = 2
 
-
-nio.logger_group.level = logbook.ERROR
-
+nio_logger = logging.getLogger("nio")
+nio_logger.setLevel(logging.ERROR)
 
 class Option(
     namedtuple(
@@ -141,18 +139,13 @@ def change_log_level(category, level):
     Called every time the user changes the log level or log category
     configuration option."""
 
+    if category == "encryption":
+        category = "crypto"
+
     if category == "all":
-        nio.logger_group.level = level
-    elif category == "http":
-        nio.http.logger.level = level
-    elif category == "client":
-        nio.client.logger.level = level
-    elif category == "events":
-        nio.events.logger.level = level
-    elif category == "responses":
-        nio.responses.logger.level = level
-    elif category == "encryption":
-        nio.crypto.logger.level = level
+        nio_logger.setLevel(level)
+    else:
+        nio_logger.getChild(category).setLevel(level)
 
 
 @utf8_decode
@@ -178,7 +171,7 @@ def config_log_level_cb(data, option):
 @utf8_decode
 def config_log_category_cb(data, option):
     """Callback for the network.debug_category option."""
-    change_log_level(G.CONFIG.debug_category, logbook.ERROR)
+    change_log_level(G.CONFIG.debug_category, logging.ERROR)
     G.CONFIG.debug_category = G.CONFIG.network.debug_category
     change_log_level(
         G.CONFIG.network.debug_category, G.CONFIG.network.debug_level
@@ -203,20 +196,20 @@ def config_pgup_cb(data, option):
     return 1
 
 
-def level_to_logbook(value):
+def level_to_logging(value):
     if value == 0:
-        return logbook.ERROR
+        return logging.ERROR
     if value == 1:
-        return logbook.WARNING
+        return logging.WARNING
     if value == 2:
-        return logbook.INFO
+        return logging.INFO
     if value == 3:
-        return logbook.DEBUG
+        return logging.DEBUG
 
-    return logbook.ERROR
+    return logging.ERROR
 
 
-def logbook_category(value):
+def logging_category(value):
     if value == 0:
         return "all"
     if value == 1:
@@ -647,7 +640,7 @@ class MatrixConfig(WeechatConfig):
                 0,
                 "error",
                 "Enable network protocol debugging.",
-                level_to_logbook,
+                level_to_logging,
                 config_log_level_cb,
             ),
             Option(
@@ -658,7 +651,7 @@ class MatrixConfig(WeechatConfig):
                 0,
                 "all",
                 "Debugging category",
-                logbook_category,
+                logging_category,
                 config_log_category_cb,
             ),
             Option(
diff --git a/matrix/globals.py b/matrix/globals.py
index c3e099e..22a8837 100644
--- a/matrix/globals.py
+++ b/matrix/globals.py
@@ -16,9 +16,9 @@
 
 from __future__ import unicode_literals
 
+import logging
 import sys
 from typing import Any, Dict, Optional
-from logbook import Logger
 from collections import OrderedDict
 
 from .utf import WeechatWrapper
@@ -44,5 +44,5 @@ ENCRYPTION = True  # type: bool
 SCRIPT_NAME = "matrix"  # type: str
 BUFFER_NAME_PREFIX = "{}.".format(SCRIPT_NAME)  # type: str
 TYPING_NOTICE_TIMEOUT = 4000  # 4 seconds typing notice lifetime
-LOGGER = Logger("weechat-matrix")
+LOGGER = logging.getLogger(__name__)
 UPLOADS = OrderedDict()  # type: Dict[str, Upload]
diff --git a/pyproject.toml b/pyproject.toml
index 7b3dca8..6690240 100644
--- a/pyproject.toml
+++ b/pyproject.toml
@@ -17,9 +17,8 @@ webcolors = "^1.11.1"
 atomicwrites = "^1.3.0"
 future = "^0.18.2"
 attrs = "^19.3.0"
-logbook = "^1.5.3"
 pygments = "^2.6.1"
-matrix-nio = { version = "^0.18.0", extras = [ "e2e" ] }
+matrix-nio = { version = "^0.21.0", extras = [ "e2e" ] }
 python-magic = { version = "^0.4.15", optional = true }
 aiohttp = { version = "^3.6.2", optional = true }
 requests = { version = "^2.23.0", optional = true }
diff --git a/requirements.txt b/requirements.txt
index 7b5fc7e..e25ad0f 100644
--- a/requirements.txt
+++ b/requirements.txt
@@ -4,9 +4,8 @@ webcolors
 future; python_version < "3.2"
 atomicwrites
 attrs
-logbook
 pygments
-matrix-nio[e2e]>=0.6
+matrix-nio[e2e]>=0.21.0
 aiohttp ; python_version >= "3.5"
 python-magic
 requests
